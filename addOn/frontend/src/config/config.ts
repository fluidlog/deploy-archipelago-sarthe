import MapIcon from '@mui/icons-material/Map';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import { LayoutOptions } from "../layouts/LayoutContext";
import AppBarTitle from '../transiscopeSarthe/AppBarTitle';
import Footer from '../transiscopeSarthe/Footer';

declare global {
  interface Window {
    VITE_MIDDLEWARE_URL: string;
    VITE_MAPBOX_ACCESS_TOKEN: string;
  }
}

interface ConfigInterface {
  middlewareUrl: string;
  mapboxAccessToken: string;
  importableResources: string[];
  title: string;
  layout: LayoutOptions;
}

const config: ConfigInterface = {
  // Middleware API url (ex: https://<host>:<port>/). Should contain a trailing slash.
  middlewareUrl: window.VITE_MIDDLEWARE_URL,

  // Mapbox Access Token used for addresses completion
  mapboxAccessToken: window.VITE_MAPBOX_ACCESS_TOKEN,

  // Displays import tab when creating resource if it is listed here
  importableResources: [],

  // Application title
  title: "Archipel Transi'Sarthe",

  // UI layout configuration
  layout: {
    name: 'topMenu',
    options: {
      logo: {
        url: '/logo192.png',
        alt: "Archipel Transi'Sarthe"
      },
      title: AppBarTitle,
      mainMenu: [
        {
          resource: 'Organization',
          label: 'Carte des alternatives',
          mobileLabel: 'Carte',
          link: '/Organization?perPage=500&sort=pair%3Alabel&view=map&lat=47.983&lng=-0.249&zoom=9',
          icon: MapIcon
        },
        {
          resource: 'Event',
          label: 'Agenda',
          link: '/Event',
          icon: CalendarMonthIcon
        }
      ],
      footer: Footer
    },
  },
};

export default config;
